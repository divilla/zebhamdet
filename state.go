package main

type (
	appState struct {
		startingPositionIndex int
	}
)

func (sp *appState) NextStartingPositionIndex() int {
	spi := sp.startingPositionIndex % totalSquares
	sp.startingPositionIndex++

	return spi
}
