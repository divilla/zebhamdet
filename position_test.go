package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestPosition_NewPosition(t *testing.T) {
	posNeg := newPosition(-1)
	assert.Equal(t, 0, posNeg.x)
	assert.Equal(t, 0, posNeg.y)

	posHigh := newPosition(totalSquares + 1)
	assert.Equal(t, 0, posHigh.x)
	assert.Equal(t, 0, posHigh.y)
}
