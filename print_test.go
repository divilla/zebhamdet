package main

import (
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

func TestPrint_PrintBoard(t *testing.T) {
	var spi int
	var squares []int
	var ok bool
	sb := new(strings.Builder)
	state := &appState{startingPositionIndex: 0}

	for {
		if spi, squares, ok = findClosedTour(state); ok {
			break
		}
	}

	printBoard(sb, spi, squares)
	assert.Equal(t, 2189, len(sb.String()))
}

func TestPrint_SquareIndexOfMove(t *testing.T) {
	var ok bool
	squares := make([]int, totalSquares)
	state := &appState{startingPositionIndex: 0}

	for {
		if _, squares, ok = findClosedTour(state); ok {
			break
		}
	}

	si := squareIndexOfMove(squares, 101)
	assert.Equal(t, -1, si)
}

func TestPrint_PositionName(t *testing.T) {
	pos := newPosition(0)
	assert.Equal(t, "A1", positionName(pos))
}
