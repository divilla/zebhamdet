package main

type (
	position struct {
		x int
		y int
	}
)

func newPosition(squareIndex int) position {
	var pos position
	if squareIndex < 0 || squareIndex >= totalSquares {
		return pos
	}

	pos.x = squareIndex%boardSize + 1
	pos.y = squareIndex/boardSize%boardSize + 1
	return pos
}

func (p position) isValid() bool {
	return p.x > 0 && p.x <= boardSize && p.y > 0 && p.y <= boardSize
}

func (p position) squareIndex() int {
	return (p.y-1)*boardSize + p.x - 1
}
