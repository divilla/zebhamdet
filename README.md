# ZebHamDet

Deterministic version of Warnsdorff’s algorithm for pawn's tour problem. 

### Run code

```shell
# list make commands
make

# run program
make run

# test
make test

# test coverage
make test-cover

# clean coverage
make clean

```

### Authors and acknowledgment
ljudevit.ocic@gmail.com

### License
MIT

### Project status
Done with 99% test coverage.
