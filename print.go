package main

import (
	"fmt"
	"strings"
)

var (
	columnNames = []string{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"}
	rowNames    = []string{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}
)

func printBoard(sb *strings.Builder, spi int, squares []int) {
	sb.WriteString(fmt.Sprintf("\r\nStarting position index: '%v'\r\n\r\n", spi))

	sb.WriteString("Moves: \r\n")
	for i := 1; i < totalSquares; i++ {
		oldPosName := positionName(newPosition(squareIndexOfMove(squares, i)))
		newPosName := positionName(newPosition(squareIndexOfMove(squares, i+1)))
		sb.WriteString(fmt.Sprintf("%2v. %s:%s\r\n", i, oldPosName, newPosName))
	}
	sb.WriteString("\r\n")

	sb.WriteString("      ")
	for i := 0; i < boardSize; i++ {
		sb.WriteString(fmt.Sprintf("    %s ", columnNames[i]))
	}
	sb.WriteString("\r\n")

	sb.WriteString("      ")
	for i := 0; i < boardSize; i++ {
		sb.WriteString("   ---")
	}
	sb.WriteString("\r\n")

	for j := boardSize - 1; j >= 0; j-- {
		sb.WriteString(fmt.Sprintf("%2s   |", rowNames[j]))
		for i := 0; i < boardSize; i++ {
			move := squares[j*boardSize+i]
			sb.WriteString(fmt.Sprintf("%6v", move))
		}
		sb.WriteString(fmt.Sprintf("   |  %2s", rowNames[j]))
		sb.WriteString("\r\n")
	}

	sb.WriteString("      ")
	for i := 0; i < boardSize; i++ {
		sb.WriteString("   ---")
	}
	sb.WriteString("\r\n")

	sb.WriteString("      ")
	for i := 0; i < boardSize; i++ {
		sb.WriteString(fmt.Sprintf("    %s ", columnNames[i]))
	}
	sb.WriteString("\r\n\r\n")
}

func squareIndexOfMove(squares []int, move int) int {
	for i := 0; i < totalSquares; i++ {
		if squares[i] == move {
			return i
		}
	}

	return -1
}

func positionName(pos position) string {
	return columnNames[pos.x-1] + rowNames[pos.y-1]
}
