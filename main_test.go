package main

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMain_RealMain(t *testing.T) {
	exitVal := realMain()
	assert.Equal(t, 0, exitVal)
}

func TestMain_Assignment(t *testing.T) {
	var spi int
	var squares []int
	var ok bool
	state := &appState{startingPositionIndex: 0}
	spiMap := map[int]int{
		0: 15,
		1: 42,
		2: 79,
	}

	for i := 0; i < 3; i++ {
		for {
			if spi, squares, ok = findClosedTour(state); ok {
				break
			}
		}

		assert.Equal(t, spiMap[i], spi)
		assert.Equal(t, len(squares), totalSquares, fmt.Sprintf("Number of squares must be equal to: '%v'", totalSquares))

		squaresMap := make(map[int]struct{})
		oldSquareIndex := squareIndexOfMove(squares, 1)
		assert.NotEqual(t, -1, oldSquareIndex, fmt.Sprintf("Move number: 1 does not exist"))
		squaresMap[1] = struct{}{}
		curPos := newPosition(oldSquareIndex)
		for i := 2; i <= totalSquares; i++ {
			newSquareIndex := squareIndexOfMove(squares, i)
			assert.NotEqual(t, -1, newSquareIndex, fmt.Sprintf("Move number %v does not exist", i))
			newPos := newPosition(newSquareIndex)
			assert.True(t, isValidMove(curPos, newPos), fmt.Sprintf("Move number %v is invalid: %v, %v", i, newPos.x-curPos.x, newPos.y-curPos.y))
			squaresMap[i] = struct{}{}
			curPos = newPos
		}
		assert.True(t, len(squaresMap) == totalSquares, fmt.Sprintf("Invalid number of moves: %v", len(squaresMap)))
	}
}

func isValidMove(oldPos, newPos position) bool {
	for i := 0; i < moves; i++ {
		if oldPos.x+moveX[i] == newPos.x && oldPos.y+moveY[i] == newPos.y {
			return true
		}
	}

	return false
}

// "In deterministic algorithm, for a given particular input,
// the computer will always produce the same output going through the same states"
// TestDeterministic proves that algorithm is deterministic by monitoring & comparing states
func TestDeterministic(t *testing.T) {
	var squares []int
	var ok bool

	// setting state.startingPositionsIndex to 0..15, 16..42, 43..79 will always yield same values
	startingPositionsMap := map[int][]int{
		0:  {14, 44, 41, 13, 45, 40, 77, 46, 39, 76, 63, 23, 16, 62, 71, 1, 61, 70, 2, 60, 42, 12, 65, 43, 98, 81, 48, 99, 82, 47, 15, 57, 72, 24, 58, 73, 78, 59, 38, 75, 64, 22, 17, 84, 66, 100, 83, 69, 3, 90, 55, 11, 30, 56, 97, 80, 49, 74, 79, 50, 18, 87, 67, 25, 88, 68, 26, 89, 37, 7, 31, 21, 96, 85, 33, 95, 92, 34, 4, 91, 54, 10, 29, 53, 9, 28, 52, 8, 27, 51, 19, 86, 32, 20, 93, 35, 5, 94, 36, 6},
		16: {16, 44, 41, 15, 45, 40, 77, 46, 39, 76, 63, 23, 2, 62, 71, 3, 61, 70, 4, 60, 42, 14, 65, 43, 98, 81, 48, 99, 82, 47, 17, 57, 72, 24, 58, 73, 78, 59, 38, 75, 64, 22, 1, 84, 66, 100, 83, 69, 5, 90, 55, 13, 30, 56, 97, 80, 49, 74, 79, 50, 18, 87, 67, 25, 88, 68, 26, 89, 37, 9, 31, 21, 96, 85, 33, 95, 92, 34, 6, 91, 54, 12, 29, 53, 11, 28, 52, 10, 27, 51, 19, 86, 32, 20, 93, 35, 7, 94, 36, 8},
		43: {46, 7, 18, 47, 8, 19, 78, 9, 20, 79, 39, 59, 72, 38, 58, 69, 37, 57, 68, 36, 17, 48, 45, 63, 98, 44, 64, 97, 43, 10, 73, 6, 55, 76, 71, 56, 77, 70, 21, 80, 40, 60, 95, 41, 85, 96, 42, 84, 67, 35, 16, 49, 74, 62, 99, 75, 65, 100, 29, 11, 90, 5, 54, 89, 33, 53, 88, 34, 22, 81, 26, 61, 94, 27, 86, 93, 28, 83, 66, 1, 15, 50, 32, 14, 51, 31, 13, 52, 30, 12, 91, 4, 25, 92, 3, 24, 87, 2, 23, 82},
	}
	state := &appState{startingPositionIndex: 0}

	for key, vals := range startingPositionsMap {
		state.startingPositionIndex = key
		for {
			if _, squares, ok = findClosedTour(state); ok {
				// this proves function is deterministic
				assert.True(t, compareSquares(vals, squares))
				break
			}
		}
	}
}

func compareSquares(expected, actual []int) bool {
	for i := 0; i < totalSquares; i++ {
		if expected[i] != actual[i] {
			return false
		}
	}

	return true
}
