package main

func makeMove(squares []int, curPos position) (position, bool) {
	var newPos position
	minDeg := moves + 1
	minDegIdx := -1

	for i := 0; i < moves; i++ {
		pos := position{
			x: curPos.x + moveX[i],
			y: curPos.y + moveY[i],
		}
		if !pos.isValid() || !isFreeSquare(squares, pos) {
			continue
		}

		if deg := degree(squares, pos); deg < minDeg {
			minDeg = deg
			minDegIdx = i
		}
	}

	if minDegIdx == -1 {
		return newPos, false
	}

	newPos.x = curPos.x + moveX[minDegIdx]
	newPos.y = curPos.y + moveY[minDegIdx]
	squares[newPos.squareIndex()] = squares[curPos.squareIndex()] + 1

	return newPos, true
}

func degree(squares []int, pos position) int {
	res := 0
	for i := 0; i < moves; i++ {
		newPos := position{
			x: pos.x + moveX[i],
			y: pos.y + moveY[i],
		}
		if newPos.isValid() && isFreeSquare(squares, newPos) {
			res++
		}
	}

	return res
}

func isFreeSquare(squares []int, pos position) bool {
	return squares[pos.squareIndex()] == 0
}
