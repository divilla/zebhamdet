package main

import (
	"fmt"
	"os"
	"strings"
)

const (
	// horizontal & vertical size of board
	boardSize = 10

	// number of squares on the board
	totalSquares = boardSize * boardSize
)

var (
	moveX = []int{3, -3, 0, 0, 2, -2, 2, -2}
	moveY = []int{0, 0, 3, -3, 2, 2, -2, -2}
	moves = len(moveX)
)

func main() {
	os.Exit(realMain())
}

func realMain() int {
	var spi int
	var squares []int
	var ok bool
	sb := new(strings.Builder)

	// app is deterministic - for each starting position index it will return the same board
	// you can test 0, 16, 43, it will find valid starting positions 15, 42, 79
	state := &appState{startingPositionIndex: 43}

	for {
		if spi, squares, ok = findClosedTour(state); ok {
			printBoard(sb, spi, squares)
			fmt.Print(sb.String())
			break
		}
	}

	return 0
}

func findClosedTour(state *appState) (int, []int, bool) {
	var zeroIndex = -1
	var zeroMoves []int
	squares := make([]int, totalSquares)

	startingPositionIndex := state.NextStartingPositionIndex()
	squares[startingPositionIndex] = 1

	var ok bool
	startingPosition := newPosition(startingPositionIndex)
	pos := startingPosition
	for i := 0; i < totalSquares-1; i++ {
		pos, ok = makeMove(squares, pos)
		if !ok {
			return zeroIndex, zeroMoves, false
		}
	}

	if !canMoveToStartingPosition(pos, startingPosition) {
		return zeroIndex, zeroMoves, false
	}

	return startingPositionIndex, squares, true
}

func canMoveToStartingPosition(pos, startPos position) bool {
	for i := 0; i < moves; i++ {
		if startPos.x == pos.x+moveX[i] && startPos.y == pos.y+moveY[i] {
			return true
		}
	}

	return false
}
